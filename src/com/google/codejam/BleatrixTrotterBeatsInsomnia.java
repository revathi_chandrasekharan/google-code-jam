package com.google.codejam;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * @author Revathi
 * Problem: Counting Sheep
 * https://code.google.com/codejam/contest/6254486/dashboard
 *
 */
public class BleatrixTrotterBeatsInsomnia {
	private static final String OUTPUT_TEMPLATE = "Case #%s: %s";
	private static final String STATIC_TEXT = "INSOMNIA";

	public static void main(String[] args) {
		Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
		int noOfInputs = in.nextInt();
		for (int index = 0; index < noOfInputs; index++) {
			long input = in.nextLong();
			if (input > 0 ) {
				Set<Integer> digits = new HashSet<>();
				int counter = 1;
				long remainder = 0L;
				while (true) {
					long value = input * counter++;
					long divident = value;
					
					while (divident > 0) {
						remainder = divident % 10;
						divident = divident / 10;
						digits.add((int) remainder);
						if (digits.size() == 10) {
							break;
						}
					}
					if (digits.size() == 10) {
						generateOutput(index, value);
						break;
					}
				}

			} else {
				generateOutput(index, -1);
			}
		}
		in.close();
	}

	private static void generateOutput(int index, long n) {
		System.out.println(String.format(OUTPUT_TEMPLATE, index + 1, (n == -1 ? STATIC_TEXT : n)));
	}

}
