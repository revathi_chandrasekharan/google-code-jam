package com.google.codejam;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
/**
 * 
 * @author Revathi
 * Problem: Oversized Pancake Flipper
 * https://code.google.com/codejam/contest/3264486/dashboard
 *
 */
public class PancakeFlipper {
	private static final String OUTPUT_TEMPLATE = "Case #%s: %s";
	public static void main(String[] args) {
		Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
		int noOfInputs = in.nextInt();
		for (int index = 0; index < noOfInputs; index++) {
			String input = in.next();
			char[] array = input.toCharArray();
			int k = Integer.parseInt(in.next());
			int noOfFlips = 0;
			for (int i = 0; i < array.length - k + 1; i++) {
				if (array[i] == '+') {
					continue;
				}
				flip(array, i, k);
				noOfFlips++;
			}
			boolean possible = true;
			for (int i = 0; i < array.length; i++) {
				if (array[i] == '-') {
					possible = false;
				}
			}
			if (possible) {
				System.out.println(String.format(OUTPUT_TEMPLATE, index+1, noOfFlips));
			} else {
				System.out.println(String.format(OUTPUT_TEMPLATE, index+1, "IMPOSSIBLE"));
			}
		}
		in.close();
	}

	private static void flip(char[] array, int i, int k) {
		for (int j = 0; j < k; j++) {
			array[i + j] = (array[i + j] == '+') ? '-' : '+';
		}
	}
}
