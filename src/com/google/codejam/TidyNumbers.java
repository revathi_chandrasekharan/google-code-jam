package com.google.codejam;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
/**
 * @author Revathi
 * Problem: Tidy Numbers
 * https://code.google.com/codejam/contest/3264486/dashboard#s=p1
 *
 */
public class TidyNumbers {
	private static final String OUTPUT_TEMPLATE = "Case #%s: %s";
	public static void main(String[] args) {
		Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
		int noOfInputs = in.nextInt();
		for (int index = 0; index < noOfInputs; index++) {
			long input = in.nextLong();
			long lastTidyNumber = 1;
			for(long j = input; j > 0 ; j--)	{
				if(j % 10 == 0)	{
					continue;
				}
				
				long divident = j;
				long remainder = j;
				while(divident > 0)	{
					long temp = divident % 10;
					if(temp > remainder)	{
						break;
					}
					remainder = temp;
					divident = divident / 10;
				}
				if(divident == 0)	{
					lastTidyNumber = j;
					break;
				} 
				
			}
			generateOutput(index, lastTidyNumber);
		}

		in.close();
	}
	
	private static void generateOutput(int index, long n) {
		System.out.println(String.format(OUTPUT_TEMPLATE, index+1, n));
	}

}
